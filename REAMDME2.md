# All the Team help from Kenzie students and Facillitators!

1. The Create User was helped by Matt Forcella and Facillitator Jake Hershey

2. My biggest coach was Ian Waddell understanding how each api should work in reducers, actions, components, screens, navigation, (ALL) indexes, and the speciality of me only writing the code.

3. Component Library is being used from R-Suite-JS.

## Retrospective Notes

1. I learned how to use x for Actions and Reducers.
2. I learned how to tie everything together from Redux, to my components, and adding the component library.
3. I learned how to map out not one but multiple pages and how they should look and how to keep the main looks integrity.
4. I learned to lean into every resource possible to learn and to get ideas of my own or even to see a bug before it became a bug.
5. I learned the why useEffect is such important syntax. (Throws the work on the screen)
6. I learned that most of this work was very repetitious but still needed a keen eye on what I worked on and what I was currently working on and cycle through.
7. How many endpoints I finished xD.
8. Thank you Kenzie for making this a project!!  


Extra Credit For Ian Waddell's team Skills

A shout out to Jonluke for helping me learn my API's faster and not to let it get in over my head.
Kenzie Fam was awesome team for a lone wolf like me, I recieved help from Jake Hershey, D.J. Drevitch, Matt Forcella, Marcus Chiriboga, and last but certainly not least Sir Derek from Student Services because his positivity just rubs off on ya makin you feel like you can do anything. Kwitter is not for Quitters but maybe for some Quakers xD
