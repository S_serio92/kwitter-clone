import { PROFILE, PROFILE_SUCCESS, PROFILE_FAILURE } from "../actions/profile";

const INITIAL_STATE = {
    users: null,
    loading: false,
    error: "",
}

export const profileReducer = (state = { ...INITIAL_STATE }, action) => {
    switch (action.type) {
        case PROFILE:
            return {
                ...INITIAL_STATE,
                loading: true,
            };
        case PROFILE_SUCCESS:
            return {
                ...INITIAL_STATE,
                loading: false,
                users: action.payload,
                error: ""
            };
        case PROFILE_FAILURE:
            return {
                ...INITIAL_STATE,
                loading: false,
                users: null,
                error: action.payload,
            };
        default:
            return state;

    }
}