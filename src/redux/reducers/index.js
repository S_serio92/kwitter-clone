import { combineReducers } from "redux";
import { authReducer } from "./auth";
import { usersReducer } from "./users";
import { profileReducer } from "./profile";
import { messagesReducer } from "./messages";
import { createMessageReducer } from "./messages";
import { updateProfileReducer } from "./updateProfile";

export default combineReducers({
    auth: authReducer,
    users: usersReducer,
    profileInfo: profileReducer,
    messages: messagesReducer,
    createMessage: createMessageReducer,
    updateProfile: updateProfileReducer,
});
