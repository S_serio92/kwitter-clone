import { MESSAGES, MESSAGES_SUCCESS, MESSAGES_FAILURE, CREATE_MESSAGE, CREATE_MESSAGE_SUCCESS, CREATE_MESSAGE_FAILURE } from "../actions";

const INITIAL_STATE = {
    messages: null,
    text: "",
    loading: false,
    error: "",
}

export const messagesReducer = (state = { ...INITIAL_STATE }, action) => {
    switch (action.type) {
        case MESSAGES:
            return {
                ...INITIAL_STATE,
                loading: true,
            };
        case MESSAGES_SUCCESS:
            return {
                ...INITIAL_STATE,
                loading: false,
                messages: action.payload,
                error: "",
            };
        case MESSAGES_FAILURE:
            return {
                ...INITIAL_STATE,
                loading: false,
                messages: null,
                error: action.payload,
            };
        default:
            return state;
    }

}

export const createMessageReducer = (state = { ...INITIAL_STATE }, action) => {
    switch (action.type) {
        case CREATE_MESSAGE:
            return {
                ...INITIAL_STATE,
                loading: true,
            };
        case CREATE_MESSAGE_SUCCESS:
            return {
                ...INITIAL_STATE,
                loading: false,
                text: action.payload,
                error: "",
            };
        case CREATE_MESSAGE_FAILURE:
            return {
                ...INITIAL_STATE,
                loading: false,
                error: action.payload,
            };
        default:
            return state;
    }

}