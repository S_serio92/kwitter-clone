import api from "../../utils/api";
export const PROFILE = "PROFILE";
export const PROFILE_SUCCESS = "PROFILE_SUCCESS";
export const PROFILE_FAILURE = "PROFILE_FAILURE";

export const profile = () => async (dispatch, getState) => {
    try {
        const username = getState().auth.username
        dispatch({ type: PROFILE });
        const payload = await api.profile(username);
        dispatch({ type: PROFILE_SUCCESS, payload });
    } catch (err) {
        dispatch({
            type: PROFILE_FAILURE,
            payload: err.message,
        });
    }
};