import api from "../../utils/api";

// AUTH CONSTANTS
export const LOGIN = "AUTH/LOGIN";
export const LOGIN_SUCCESS = "AUTH/LOGIN_SUCCESS";
export const LOGIN_FAILURE = "AUTH/LOGIN_FAILURE";
export const LOGOUT = "AUTH/LOGOUT";
export const SIGN_UP = "AUTH/SIGN_UP";
export const SIGN_UP_SUCCESS = "AUTH/SIGN_UP_SUCCESS";
export const SIGN_UP_FAILURE = "AUTH/SIGN_UP_FAILURE";
export const GOOGLE_LOGIN = "AUTH/GOOGLE_LOGIN";
export const GOOGLE_LOGIN_SUCCESS = "AUTH/GOOGLE_LOGIN_SUCCESS";
export const GOOGLE_LOGIN_FAILURE = "AUTH/GOOGLE_LOGIN_FAILURE";


/*
 AUTH ACTIONS (this is a thunk....)
 THUNKS: --> https://github.com/reduxjs/redux-thunk#whats-a-thunk
 If you need access to your store you may call getState()
*/

const googleLogin = (data) => {
  return {
    type: GOOGLE_LOGIN_SUCCESS,
    payload: data,
  }
}

const login = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: LOGIN });
    const payload = await api.login(credentials);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: LOGIN_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: LOGIN_FAILURE,
      payload: err.message,
    });
  }
};

const logout = () => async (dispatch, getState) => {
  try {
    // We do not care about the result of logging out
    // as long as it succeeds
    await api.logout();
  } finally {
    /**
     * Let the reducer know that we are logged out
     */
    dispatch({ type: LOGOUT });
  }
};

const newUser = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: SIGN_UP })
    const payload = await api.newUser(credentials);
    dispatch({ type: SIGN_UP_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: SIGN_UP_FAILURE,
      payload: err.message,
    });
  }
};


export const actions = {
  login,
  logout,
  newUser,
  googleLogin,
};
