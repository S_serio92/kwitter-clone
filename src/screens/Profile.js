import React from "react";
import { MenuContainer, ProfileContainer } from "../components";


export const ProfileScreen = () => (
  <>
    <MenuContainer />
    <h2>Profile</h2>
    <ProfileContainer />
  </>
);
