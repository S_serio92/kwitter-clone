import React from "react";
import { Link } from "react-router-dom";

const NotFound = ({ location }) => (
  <>
    <p>Page not found for {location.pathname}</p>
    <img src="/mascot.png" alt="" />
    <Link to="/">Go Home</Link>
  </>
);

export const NotFoundScreen = NotFound;
