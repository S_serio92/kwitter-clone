import React from "react";
import { UpdateProfileContainer, MenuContainer } from "../components";

export const UpdateProfileScreen = () => (
    <React.Fragment>
    <MenuContainer />
    <h2>Update Profile</h2>
    <UpdateProfileContainer />
    </React.Fragment>
)