import React from "react";
import { MenuContainer, MessagesContainer } from "../components";


export const MessageScreen = () => (
    <>
        <MenuContainer />
        <h2>Message Feed</h2>
        <MessagesContainer />
    </>
);