import React from "react";
import { LoginFormContainer, MenuContainer, CreateUserContainer } from "../components";


export const HomeScreen = () => (
  <>
    <MenuContainer />
    
    <h2 align="center">Your favorite microblogging platform</h2>

    <LoginFormContainer />
<CreateUserContainer />
  </>
);
