import React from "react";
import { UsersContainer, MenuContainer } from "../components/";

export const UsersScreen = () => (
    <>
    <MenuContainer />
    <h1>Users</h1>
        <UsersContainer />
    </>
)