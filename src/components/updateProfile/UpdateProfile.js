import React from "react";
import { useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import api from "../../utils/api";
import { Button } from "rsuite";
import { profile } from "../../redux/actions/profile";
import { updateProfile } from "../../redux/actions/updateProfile";


export const UpdateProfile = () => {
    const user = useSelector((state) => state.auth.username);
    const dispatch = useDispatch();
    const picture = useRef(null);
    const [state, setState] = useState({
        displayName: "",
        about: "",
        password: "",
    })

    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(updateProfile(state));
        setState({ password: "", about: "", displayName: "" });


    };

    const handlePicture = async (event) => {
        event.preventDefault();
        const picUrl = new FormData(picture.current);
        const results = await api.propic(user, picUrl);
        dispatch(profile(results));
    }


    const handleChange = (event) => {
        const inputName = event.target.name;
        const inputValue = event.target.value;
        setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
    }; 

    return (
        <React.Fragment>
            <form onSubmit={handleSubmit}>
                <label htmlFor="displayName">
                    New Name
                </label>
                <input
                    type="text"
                    name="displayName"
                    value={state.displayName}
                    onChange={handleChange}
                />
                <label htmlFor="about">
                    Change About
                </label>
                <input
                    type="text"
                    name="about"
                    value={state.about}
                    onChange={handleChange}
                />
                <label htmlFor="password">
                    New Password
                </label>
                <input
                    type="password"
                    name="password"
                    value={state.password}
                    onChange={handleChange}
                />
                <Button appearance="default" type="submit">Submit</Button>
            </form>
            <form onSubmit={handlePicture} ref={picture}>
                <input
                    type="file"
                    name="picture"
                />
                <Button appearance="default" type="submit">Set Profile Pic</Button>
            </form>
        </React.Fragment>
    )
}
