import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import { HomeScreen, ProfileScreen, NotFoundScreen, UsersScreen, MessageScreen, CreateMessageScreen,UpdateProfileScreen } from "../../screens";
import { ConnectedRoute } from "../connected-route/ConnectedRoute";



export const Navigation = () => (
  <BrowserRouter>
    <Switch>
      <ConnectedRoute
        exact
        path="/"
        redirectIfAuthenticated
        component={HomeScreen}
      />
      <ConnectedRoute
        exact
        isProtected
        path="/profiles/:username"
        component={ProfileScreen}
      />
      <ConnectedRoute
        exact 
        path="/users"
        component={UsersScreen}
      />

      <ConnectedRoute
        exact 
        path="/messagefeed"
        component={MessageScreen}
      />

      
      <ConnectedRoute exact path="/createmessage" component={CreateMessageScreen} />

      <ConnectedRoute exact path="/updateprofile" component={UpdateProfileScreen} />

      <ConnectedRoute path="*" component={NotFoundScreen} />
      
    </Switch>
  </BrowserRouter>
);
