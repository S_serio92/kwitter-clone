import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { createMessage } from "../../redux/actions/messages";
import { Button } from "rsuite";

export const CreateMessage = () => {
    const dispatch = useDispatch();
    const [state, setState] = useState({ text: "" });

    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(createMessage(state));
        setState({ text: "" });


    };

    const handleChange = (event) => {
        const inputName = event.target.name;
        const inputValue = event.target.value;
        setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
    };    

    return (
        <>
            <form onSubmit={handleSubmit}>
                
                <label htmlFor="text">
                    Create Message
                </label>
                <input
                    type="text"
                    name="text"
                    value={state.text}
                    required
                    onChange={handleChange} 
                />
                <Button appearance="default" type="submit">Send</Button>
       </form>
        
            
        </>
)
 
    


    
}

