import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { users } from "../../redux/actions/users"


    
    
export const Users = () => {
    const usersList = useSelector((state) => state.users.users);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(users());
}, [dispatch]);
    console.log(usersList)
    return (
        <React.Fragment>
            {usersList && (usersList.users.map((data) => (
               <>
               <div>
                    User Name: {data.username}
                    <br />
                    Display Name: {data.displayName}
                    <br />
                    Created At: {data.createdAt}
                </div>
                <br />
                </>
            )))}
        </React.Fragment>
    )
}