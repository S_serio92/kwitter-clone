import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { profile } from "../../redux/actions/profile";
import "rsuite/dist/styles/rsuite-default.css";
import { Button, Panel } from "rsuite";
export const Profile = () => { 
    const profileData = useSelector((state) => state.profileInfo.users)
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(profile());
    }, [dispatch]);



    console.log(profileData)
    return (
        <React.Fragment>
        { profileData && (
            <>
                <Panel shaded bordered bodyFill style={{ display: "inline-block", width: 240 }}>
                <img
                    src={"https://kwitter-api.herokuapp.com" + profileData.user.pictureLocation}
                    width="200px"
                    height="200px"
                    alt="profile pic"
                />
                <Panel header={"Display Name: " + profileData.user.displayName}>
                    <p>
                    <strong>User Name: {profileData.user.username}
                    <br />
                    About: {profileData.user.about}
                    <br />
                    createdAt: {profileData.user.createdAt}
                    </strong>
                    </p>
                </Panel>
                <Link to="/updateProfile">
                <Button appearance="default" block>Update Profile</Button>
            </Link>
                </Panel>;
                
            </>
            )}

        </React.Fragment>
    )
}