import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/auth";
import { Loader } from "../loader";
import { Button } from "rsuite";
import "./CreateUser.css";

export const CreateUser = ({ newUser }) => {
    const { loading, error } = useSelector((state) => ({
        loading: state.auth.loading,
        error: state.auth.error,
    }));


    const dispatch = useDispatch();

    const [state, setState] = useState({
        username: "",
        displayName: "",
        password: "",
    });

    const handleCreateUser = (event) => {
        event.preventDefault();
        dispatch(actions.newUser(state));
        
        
    };

    const handleChange = (event) => {
        const inputName = event.target.name;
        const inputValue = event.target.value;
        setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
    };

    return (
        <React.Fragment>
            <form id="create-user" onSubmit={handleCreateUser}>
                <label htmlFor="username">Username</label>
                <input
                    type="text"
                    name="username"
                    value={state.username}
                    autoFocus
                    required
                    onChange={handleChange}
                />
                <br/>
                <label htmlFor="password" >Password</label>
                < input
                    type="password"
                    name="password"
                    value={state.password}
                    autoFocus
                    required
                    onChange={handleChange}
                />
                <label htmlFor="displayName" >Display Name</label>
                
                <input
                    type="text"
                    name="displayName"
                    value={state.displayName}
                    autoFocus
                    required
                    onChange={handleChange}
                    />
             <br/>
                <Button appearance="defualt" type="submit" disabled={loading}>
                    Sign Up
        </Button>
            </form>
            {loading && <Loader />}
            {error && <p style={{ color: "red" }}>{error.message}</p>}
        </React.Fragment>
    );
};
