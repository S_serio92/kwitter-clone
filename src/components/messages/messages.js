import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { messages } from "../../redux/actions/messages";

export const Messages = () => {
    const messageFeed = useSelector((state) => state.messages.messages);

    const dispatch = useDispatch();
  
    useEffect(() => {
      dispatch(messages());
    }, [dispatch]);

    return (
        <React.Fragment>
            {messageFeed && (messageFeed.messages.map((data) => (
                <>
                <div>
                    Username: {data.username}
                    <br />
                    Messages: {data.text}
                    <br />
                    Posted on: {data.createdAt}
                </div>
                <br />
                </>
            )))}
        </React.Fragment>
    )
}